DROP TABLE IF EXISTS order_row;
DROP TABLE IF EXISTS orders;

DROP SEQUENCE IF EXISTS order_seq;
DROP SEQUENCE IF EXISTS order_row_seq;

CREATE SEQUENCE order_seq START WITH 1000;
CREATE SEQUENCE order_row_seq START WITH 5000;

CREATE TABLE orders (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('order_seq'),
    order_number VARCHAR(255) NOT NULL
);

CREATE TABLE order_row (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('order_row_seq'),
    item_name VARCHAR(255) NOT NULL,
    quantity INTEGER,
    price INTEGER,
    order_id BIGINT,
    FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE
);
