package dao;

import domain.order.Order;
import domain.order.OrderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class OrderDao {
    private static final Logger LOGGER = Logger.getLogger( OrderDao.class.getName() );

    private JdbcTemplate template;

    @Autowired
    public OrderDao(JdbcTemplate template) {
        this.template = template;
    }

    public Order insertOrder(Order order) {
        Long savedOrderId = insertOrderWithOrderRows(order);

        Order savedOrder = getOrderById(savedOrderId);
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "Returning " + savedOrder.toString()); }

        return savedOrder;
    }

    private Long insertOrderWithOrderRows(Order order) {
        var data = Map.of("order_number", order.getOrderNumber());
        Number savedOrderId = new SimpleJdbcInsert(template)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data);

        String sql = "INSERT INTO order_row (item_name, quantity, price, order_id) VALUES (?,?,?,?)";
        template.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                OrderRow orderRow = order.getOrderRows().get(i);
                ps.setString(1, orderRow.getItemName());
                ps.setInt(2,orderRow .getQuantity());
                ps.setInt(3,orderRow .getPrice());
                ps.setLong(4, (Long) savedOrderId);
            }

            @Override
            public int getBatchSize() {
                return (order.getOrderRows() != null) ? order.getOrderRows().size() : 0;
            }
        });

        return (Long) savedOrderId;
    }

    public Order getOrderById(Long id) {
        List<Order> orders = template.query("select * from orders where id = ?",
                new Object[] {id}, (resultSet, i) -> toOrder(resultSet));

        Order savedOrder = null;
        if (orders.size() == 1) {
            savedOrder = orders.get(0);
        }

        if (savedOrder != null) {
            List<OrderRow> orderRows = template.query("select * from order_row where order_id = ?",
                    new Object[] {savedOrder.getId()}, (resultSet, i) -> toOrderRow(resultSet));
            savedOrder.setOrderRows(orderRows);
        }

        return savedOrder;
    }

    private Order toOrder(ResultSet resultSet) throws SQLException {
        return Order
                .builder()
                .id(resultSet.getLong("id"))
                .orderNumber(resultSet.getString("order_number"))
                .build();
    }

    private OrderRow toOrderRow(ResultSet resultSet) throws SQLException {
        return OrderRow
                .builder()
                .itemName(resultSet.getString("item_name"))
                .quantity(resultSet.getInt("quantity"))
                .price(resultSet.getInt("price"))
                .build();
    }

    public List<Order> getAllOrders() {
        String sql = "select * from orders";
        return template.query(sql, new BeanPropertyRowMapper<>(Order.class));
    }

    public void deleteOrderById(Long id) {
        //TODO

    }
}
