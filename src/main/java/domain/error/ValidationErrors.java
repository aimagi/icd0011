package domain.error;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class ValidationErrors {
    private List<ValidationError> errors;
}
