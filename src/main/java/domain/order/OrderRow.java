package domain.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderRow {
    private String itemName;
    @NotNull
    @Min(value = 1)
    private Integer quantity;
    @NotNull
    @Min(value = 1)
    private Integer price;
}
