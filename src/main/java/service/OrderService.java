package service;

import dao.OrderDao;
import domain.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.Util;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class OrderService {
    private static final Logger LOGGER = Logger.getLogger( OrderService.class.getName() );

    private final OrderDao orderDao;

    @Autowired
    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public Order handlePostOrder(Order order) {
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "handleOrderPost request with order " + Util.parseObjectAsJson(order)); }

        Order newOrder = orderDao.insertOrder(order);
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "Inserted new order " + newOrder.toString()); }

        return newOrder;
    }


    public Order handleGetOrder(Long orderId) {
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "handleGetOrder request with id " + orderId); }

        return orderDao.getOrderById(orderId);
    }

    public List<Order> handleGetAllOrders() {
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "handleGetAllOrders request"); }

        return orderDao.getAllOrders();
    }

    public void handleDeleteOrder(Long id) {
        if (LOGGER.isLoggable(Level.INFO)) { LOGGER.log(Level.INFO, "handleOrderDelete request"); }

        orderDao.deleteOrderById(id);
    }
}
