package service;

import domain.error.ValidationError;
import domain.error.ValidationErrors;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ValidationService {

    public ValidationErrors getTooShortError() {
        ValidationErrors errors = new ValidationErrors();
        ValidationError error = new ValidationError();
        error.setCode("too_short_number");
        List<ValidationError> errorList = new ArrayList<>();
        errorList.add(error);
        errors.setErrors(errorList);

        return errors;
    }

    public ValidationErrors getErrorsFrom(List<FieldError> fieldErrors) {
        ValidationErrors errorList = new ValidationErrors();
        List<ValidationError> validationErrors = new ArrayList<>();

        for (FieldError fieldError : fieldErrors) {
            List<String> args = Arrays.stream(fieldError.getArguments()).map(Object::toString).collect(Collectors.toList());
            ValidationError error = ValidationError.builder()
                    .code(fieldError.getCode())
                    .arguments(args)
                    .build();
            validationErrors.add(error);
        }
        errorList.setErrors(validationErrors);

        return errorList;
    }
}
