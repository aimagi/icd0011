package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Util {
    private static final Logger LOGGER = Logger.getLogger( Util.class.getName() );

    public static String readStream(InputStream is) {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(is));

        return buffer.lines().collect(Collectors.joining(System.lineSeparator()));
    }

    public static List<String> unfoldAndSplit(String input) {
        input = input.replace("{", "");
        input = input.replace("}", "");
        return Arrays.asList(input.split(","));
    }

    public static String reverseString(String input) {
        StringBuilder result = new StringBuilder();
        char[] inputArray = input.toCharArray();
        int i = inputArray.length - 1;
        while (i >= 0) {
            result.append(inputArray[i]);
            i--;
        }
        return result.toString();
    }

    public static String wrapLine(String key, String value) {
        return key + ": " + value;
    }

    public static String joinLines(List<String> input) {
        StringBuilder result = new StringBuilder("{ ");
        for (int i = 0; i < input.size(); i++) {
            result.append(input.get(i));
            if (i < input.size() - 1) {
                result.append(", ");
            }
        }
        result.append(" }");

        return result.toString();
    }

    public static <T> T parseJsonAsObject(String json, Class<T> clazz) {
        T result = null;
        try {
            result = new ObjectMapper().readValue(json, clazz);
        } catch (IOException e) {
            if (LOGGER.isLoggable(Level.SEVERE)) { LOGGER.log(Level.SEVERE, "Error parsing json to object" + e.getMessage()); }
        }
        return result;
    }

    public static <T> String parseObjectAsJson(T object) {
        String result = "";
        try {
            result = new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            if (LOGGER.isLoggable(Level.SEVERE)) { LOGGER.log(Level.SEVERE, "Error parsing object to json" + e.getMessage()); }
        }
        return result;
    }
}
