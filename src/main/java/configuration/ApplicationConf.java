package configuration;

import dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import service.OrderService;
import service.ValidationService;

import javax.sql.DataSource;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "service", "controller" })
@PropertySource("classpath:/application.properties")
public class ApplicationConf {

    @Autowired
    private Environment env;

    @Bean
    public OrderService orderService() {
        OrderDao orderDao = new OrderDao(getTemplate(dataSource()));
        return new OrderService(orderDao);
    }

    @Bean
    public ValidationService validationService() {
        return new ValidationService();
    }

    @Bean
    public OrderDao orderDao(JdbcTemplate template) {
        return new OrderDao(template);
    }

    @Bean
    public JdbcTemplate getTemplate(DataSource dataSource) {
        var populator = new ResourceDatabasePopulator(new ClassPathResource("schema.sql"));
        DatabasePopulatorUtils.execute(populator, dataSource);

        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUsername(env.getProperty("dbUser"));
        ds.setPassword(env.getProperty("dbPassword"));
        ds.setUrl(env.getProperty("dbUrl"));
        return ds;
    }
}


