package controller;

import domain.order.Order;
import domain.error.ValidationErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import service.OrderService;
import service.ValidationService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrdersController {

    private final OrderService orderService;
    private final ValidationService validationService;

    public OrdersController(OrderService orderService, ValidationService validationService) {
        this.orderService = orderService;
        this.validationService = validationService;
    }

    @PostMapping(value = "orders")
    public ResponseEntity insertOrder(@RequestBody @Valid Order order) {
        Order savedOrder = orderService.handlePostOrder(order);
        return ResponseEntity.ok(savedOrder);
    }

    @GetMapping("orders/{orderId}")
    public ResponseEntity getEntity(@PathVariable("orderId") Long orderId) {
        Order order = orderService.handleGetOrder(orderId);
        return ResponseEntity.ok(order);
    }

    @GetMapping("orders")
    public ResponseEntity getAllOrders() {
        List<Order> orders = orderService.handleGetAllOrders();
        return ResponseEntity.ok(orders);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationErrors handleMethodArgumentNotValid( MethodArgumentNotValidException exception) {
        ValidationErrors errors = validationService.getErrorsFrom(exception.getBindingResult().getFieldErrors());
        return errors;
    }
}
